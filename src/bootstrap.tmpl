# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:

include:
  - local: '/templates/{{bootstrap_distro}}.yml'

stages:
  - bootstrapping
  - bootstrapping manifest
  - bootstrapping_qemu

#################################################################
#                                                               #
#                    bootstrapping stage                        #
#                                                               #
#################################################################


.bootstrap_skeleton:
  extends: .fdo.container-build@{{bootstrap_distro}}
  stage: bootstrapping
  variables:
    FDO_UPSTREAM_REPO: freedesktop/ci-templates
    FDO_DISTRIBUTION_VERSION: '{{bootstrap_distro_version}}'
    FDO_DISTRIBUTION_TAG: '{{bootstrap_tag}}'


# Builds an image that the .fdo.container-build@ templates run on
# to compose a distribution-specific image.
#
# we need a minimalist image capable of buildah, podman, skopeo, curl,
# jq, date and test. We used to rely on `bootstrap/bootstrap.sh`, but
# a commit in runc prevented it to be compiled against musl. So we just
# end up building a regular container image.
bootstrap@x86_64:
  extends: .bootstrap_skeleton
  image: {{bootstrap_distro}}:{{bootstrap_distro_version}}
  before_script:
    - bash bootstrap/bootstrap_{{bootstrap_distro}}.sh
  variables:
    FDO_REPO_SUFFIX: x86_64/buildah
    FDO_DISTRIBUTION_EXEC: 'bash bootstrap/bootstrap_{{bootstrap_distro}}.sh'


# same but for aarch64
bootstrap@aarch64:
  extends: bootstrap@x86_64
  image: arm64v8/{{bootstrap_distro}}:{{bootstrap_distro_version}}
  tags:
    - aarch64
  variables:
    FDO_REPO_SUFFIX: aarch64/buildah

bootstrap@local-manifest:
  extends:
    - .bootstrap_skeleton
  image: $CI_REGISTRY_IMAGE/x86_64/buildah:{{bootstrap_tag}}
  stage: bootstrapping manifest
  script:
    # log in to the registry
    - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

    # create the multi-arch manifest
    - buildah manifest create buildah:{{bootstrap_tag}}
              $CI_REGISTRY_IMAGE/x86_64/buildah:{{bootstrap_tag}}
              $CI_REGISTRY_IMAGE/aarch64/buildah:{{bootstrap_tag}}

    # check if we already have this manifest in the registry
    - buildah manifest inspect buildah:{{bootstrap_tag}} > new_manifest.json
    - buildah manifest inspect docker://$CI_REGISTRY_IMAGE/buildah:{{bootstrap_tag}} > current_manifest.json || true

    - diff -u current_manifest.json new_manifest.json || touch .need_push

    # and push it
    - |
      if [[ -e .need_push ]]
      then
        rm .need_push
        buildah manifest push --format v2s2 --all \
              buildah:{{bootstrap_tag}} \
              docker://$CI_REGISTRY_IMAGE/buildah:{{bootstrap_tag}}
      fi
  needs:
    - bootstrap@aarch64
    - bootstrap@x86_64


# qemu container base image. This is the base image for running qemu jobs,
# i.e. when you are building an image with .fdo.qemu-prep@, that image is a gcow
# file inside this image here, to be started with the vmctl script.
#
# Installed required packages (in addition to the bootstrap ones):
# - qemu (of course)
# - genisoimage (to create a cloud-init iso that will help us filling in the custom parameters)
# - usbutils (for being able to call lsusb and redirect part a USB device)
bootstrap-qemu@x86_64:
  extends: .bootstrap_skeleton
  image: $CI_REGISTRY_IMAGE/x86_64/buildah:{{bootstrap_tag}}
  stage: bootstrapping_qemu
  dependencies: []
  variables:
    FDO_DISTRIBUTION_TAG: '{{qemu_tag}}'
    FDO_BASE_IMAGE: $CI_REGISTRY_IMAGE/x86_64/buildah:{{bootstrap_tag}}
    FDO_REPO_SUFFIX: x86_64/qemu-base
    FDO_DISTRIBUTION_PACKAGES: 'qemu genisoimage usbutils'
    FDO_DISTRIBUTION_EXEC: 'mkdir -p /app && cp bootstrap/vmctl.sh /app/vmctl'
  needs:
    - bootstrap@x86_64

# qemu container capable of creating an other VM image. This is the image the
# .fdo.qemu-prep@ templates themselves run on.
bootstrap-qemu-mkosi@x86_64:
  extends: bootstrap-qemu@x86_64
  tags:
    - kvm
  variables:
    FDO_REPO_SUFFIX: x86_64/qemu-mkosi-base
    FDO_DISTRIBUTION_EXEC: 'mkdir -p /app && cp bootstrap/vmctl.sh /app/vmctl && bootstrap/prep_mkosi.sh'
