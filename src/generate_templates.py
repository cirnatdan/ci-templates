#!/bin/env python3

import yaml
import re
import sys
import tempfile

from pathlib import Path

sys.path.insert(0, './tools')
import ci_fairy  # noqa


def generate_template(configs, dest, template):
    '''
    Invoke ci-fairy generate-template with the given arguments.

    .. param:: configs
        An array of tuples of type (path, rootnode).

    '''
    config_args = []
    for config, root in configs:
        config_args += ['--config', f'{config}']
        if root is not None:
            config_args += ['--root', f'{root}']

    args = ['generate-template'] + config_args + ['--output-file', f'{dest}', f'{template}']
    ci_fairy.main(args, standalone_mode=False)


if __name__ == '__main__':
    src_folder = Path('src')
    config_folder = src_folder / 'config'
    ci_folder = Path('.gitlab-ci')
    ci_folder.mkdir(exist_ok=True)
    globals_file = config_folder / 'ci-globals.yml'

    def distribution_files():
        for f in config_folder.iterdir():
            if f.name.endswith('.yml') and not f.name.startswith('ci-'):
                yield f

    for distrib in distribution_files():
        name = distrib.name[:-4]  # drop .yml

        # generate the distribution's template file,
        # e.g. /templates/alpine.yml
        template = src_folder / 'distribution-template.tmpl'
        if distrib.name.startswith('os-'):
            template = src_folder / 'os-template.tmpl'
        dest = Path('templates') / f'{name}.yml'
        config = [(f'{globals_file}', ''), (f'{distrib}', f'{name}')]
        generate_template(configs=config, dest=dest, template=template)

        # generate our CI file, e.g. /.gitlab-ci/alpine-ci.yml
        template = src_folder / 'distribution-template-ci.tmpl'
        dest = ci_folder / f'{name}-ci.yml'
        generate_template(configs=config, dest=dest, template=template)

    # Generate the bootstrap-ci file
    dest = ci_folder / 'bootstrap-ci.yml'
    template = src_folder / 'bootstrap.tmpl'
    config = [(f'{globals_file}', None)]
    generate_template(configs=config, dest=dest, template=template)

    # Generate the ci-fairy template and its ci file
    ci_fairy_file = config_folder / 'ci-fairy.yml'
    template = src_folder / 'ci-fairy.tmpl'
    dest = Path('templates') / 'ci-fairy.yml'
    config = [(f'{globals_file}', ''), (f'{ci_fairy_file}', 'ci-fairy')]
    generate_template(configs=config, dest=dest, template=template)

    template = src_folder / 'ci-fairy-ci.tmpl'
    dest = ci_folder / 'ci-fairy-ci.yml'
    generate_template(configs=config, dest=dest, template=template)

    # We've generated all the templates, search for any
    # full image reference $registry/$path:$imagespec anywhere in your
    # templates and add that to the remote_images so we can test those
    # during the CI pipeline
    with open(globals_file) as fd:
        config = yaml.load(fd, Loader=yaml.Loader)
    registry, path = config['ci_templates_registry'], config['ci_templates_registry_path']
    pattern = re.compile(f'^[^#]+({registry}{path}:[\\w:.-]+)')
    remote_images = []
    for template in Path('templates/').glob('*.yml'):
        for line in open(template):
            matches = pattern.match(line)
            if matches:
                remote_images.append(matches[1])

    # write out a temporary config file with the remote images and the
    # distribution names. Use that to generate our .gitlab-ci.yml file
    with tempfile.NamedTemporaryFile() as new_config:
        s = '", "'.join(sorted(set(remote_images)))
        new_config.write(f'remote_images: ["{s}"]\n'.encode('utf8'))

        distributions = [d.name[:-4] for d in distribution_files()]
        s = '", "'.join(sorted(set(distributions)))
        new_config.write(f'distribs: ["{s}"]\n'.encode('utf8'))
        new_config.flush()

        # now generate the .gitlab-ci.yml file
        dest = Path('.gitlab-ci.yml')
        template = src_folder / 'gitlab-ci.tmpl'
        config = [(f'{globals_file}', ''),
                  (f'{ci_fairy_file}', 'ci-fairy'),
                  (f'{new_config.name}', '')]
        generate_template(configs=config, dest=dest, template=template)
