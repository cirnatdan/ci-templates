.. _templates:

Hacking on the CI Templates
===========================

This is an outline of how everything fits together in this repository.

This repository contains both the source **and** the generated
output. The directory structure is as follows:

- ``bootstrap``: Scripts used by the bootstrap images.
- ``src``: The source template files and per-distribution configuration.
- ``templates``: The **generated** templates. Do not edit.
- ``test``: Scripts used by the CI to test images.
- ``tools``: The :ref:`ci-fairy <ci-fairy>` tool.

The ``ci-templates`` repository has two consumables:
- the files in the ``templates`` folder are to be included by external
  projects, see :ref:`templates` for the details.
- the CI pipeline produces several container images that these templates
  rely on. These images are hardcoded in the templates and are the ones used
  to build the real jobs in the external project. Some of these images are
  mirrored to https://quay.io to reduce bandwidth requirements on our
  infrastructure.

When templates change, the ``src/generate_templates.py`` script generates
both the resulting ``templates/`` **and** the ``.gitlab-ci.yml`` file that
generates the consumable images and the various CI pipelines to test these
images and the templates.

The consumable images are built in the ``bootstrap`` phase of the CI
pipeline, see the ``.gitlab-ci/bootstrap-ci.yml`` file for details.
As a short summary: this stage builds container images that are capable of
building the images the external projects need.
